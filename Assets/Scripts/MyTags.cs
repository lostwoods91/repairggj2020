﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

﻿public class MyTags
{
    public static string DYING_ANIMATION = "Dying";
    public static string SUFFERING_ANIMATION = "Suffering";


    public static string IDLE_ANIMATION = "Idle";

    public static int MAX_CRITICAL_OXYGEN = 3;

    public static string SEED_TEXT_TAG = "SeedText";
    public static string OXIGEN_PLANT_NAME = "OxygenPlant";
    public static string FRUIT_PLANT_NAME = "FruitPlant";

    public static string FRUIT_ICON = "FruitTreeIcon";
    public static string OXIGEN_ICON = "OxygenTreeIcon";
    public static string FRUIT_COST = "FruitTreeCost";
    public static string OXIGEN_COST = "OxygenTreeCost";

    public static string OXIGEN_TREE_TAG = "OxygenTree";
    public static string FRUIT_TREE_TAG = "FruitTree";


}
