﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private AudioSource MyAudioS;
    private GameManager GM;
    private Plant plantType = null;
    private Camera cam;

    private Text
        SeedCountText,
        OxygenCostText,
        FruitCostText;

    private Sprite
        OxygenSprite,
        FruitSprite;

    [HideInInspector]
    public int
        OxygenCostIncrease,
        FruitCostIncrease;

    public int
        OxygenIncreaseRate,
        FruitIncreaseRate;

    public AudioClip
        ErrorSound,
        DigSound;

    // Start is called before the first frame update
    void Awake()
    {
        MyAudioS = GetComponent<AudioSource>();
        GM = FindObjectOfType<GameManager>();
        cam = Camera.main;
        SeedCountText = GameObject.FindGameObjectWithTag(MyTags.SEED_TEXT_TAG).GetComponent<Text>();
        OxygenCostText = GameObject.FindGameObjectWithTag(MyTags.OXIGEN_COST).GetComponent<Text>();
        FruitCostText = GameObject.FindGameObjectWithTag(MyTags.FRUIT_COST).GetComponent<Text>();
        OxygenSprite = GameObject.FindGameObjectWithTag(MyTags.OXIGEN_ICON).GetComponent<Sprite>();
        FruitSprite = GameObject.FindGameObjectWithTag(MyTags.FRUIT_ICON).GetComponent<Sprite>();

    }

    // Update is called once per frame
    void Update()
    {
        //if ho una pianta
        //Debug.Log(plantType);
        if (plantType != null)
        {
            string plantTypeTag = plantType.tag;
            int increasingCost = 0;
            if (plantTypeTag == MyTags.FRUIT_TREE_TAG) increasingCost = FruitCostIncrease;
            if (plantTypeTag == MyTags.OXIGEN_TREE_TAG) increasingCost = OxygenCostIncrease;

            if (Input.GetMouseButtonDown(0))
            {


                //Debug.Log("we have plant");
                Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                RaycastHit2D HitInfo = Physics2D.GetRayIntersection(ray);
                if (HitInfo)
                {
                    if (plantType.SeedPrice + increasingCost <= GM.SeedNumber)
                    {
                        //ottengo coordinate
                        Tile currTile = HitInfo.collider.gameObject.GetComponent<Tile>();

                        if (GM.CanPlant(currTile))
                        {
                            //game manager -> pianta in coordinate 
                            var plantTypeTemp = (plantType);
                            GM.PlantTree(plantTypeTemp, currTile);


                            if (plantTypeTag == MyTags.FRUIT_TREE_TAG)
                            {
                                GM.SeedNumber -= (plantType.SeedPrice + FruitCostIncrease);
                                FruitIncreaseRate = (int)(FruitIncreaseRate * 1.2f);
                                FruitCostIncrease += FruitIncreaseRate;
                                FruitCostText.text = (FruitCostIncrease + plantType.SeedPrice) + "";
                            }

                            if (plantTypeTag == MyTags.OXIGEN_TREE_TAG)
                            {
                                GM.SeedNumber -= (plantType.SeedPrice + OxygenCostIncrease);

                                OxygenCostIncrease += OxygenIncreaseRate;
                                OxygenCostText.text = (OxygenCostIncrease + plantType.SeedPrice) + "";

                            }
                            SeedCountText.text = GM.SeedNumber + "";

                            plantType = null;

                            MyAudioS.clip = DigSound;
                            MyAudioS.volume = 0.75f;
                            MyAudioS.Play();
                            MyAudioS.Play();
                        }


                    }

                    else
                    {
                        MyAudioS.clip = ErrorSound;
                         MyAudioS.volume = 0.75f;
                         MyAudioS.Play();
                        MyAudioS.Play();
                    }
                }

                
            }




        }
    }

    public void setPlantType(Plant plantType)
    {
        //Debug.Log("pianta settata");
        this.plantType = plantType;
    }


}
