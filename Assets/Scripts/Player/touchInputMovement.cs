﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class touchInputMovement : MonoBehaviour
{

    public float zoomScrollWheelMultiplicator = 1.0f;
    public float zoomTouchMultiplicator = 1.0f;
    public float scaleMin = 10.0f;
    public float scaleMax = 1.0f;
    public float groundZ = 0;
    public float KeySpeedScale = 0.1f;

    private Vector3 position;
    private Vector3 newPosition;
    private Camera cam;
    private Plane ground;
    private int layer_mask;
    private GameManager GM;
    private float maxX;
    private float maxY;

    // Start is called before the first frame update
    public void Initialize()
    {
        GM = FindObjectOfType<GameManager>();
        cam = Camera.main;
        Debug.Log(GM.TileSize);
        Debug.Log(GM.FieldSize.x);
        maxX = GM.FieldSize.x* GM.TileSize;
        maxY = GM.FieldSize.y* GM.TileSize;
        //ground = new Plane(Vector3.up, new Vector3(0, 0, groundZ));
        //layer_mask = LayerMask.GetMask("cameraCollider");
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.touchCount > 0)
        {
            /*if (Input.touchCount == 2) //TODO: se muovo e lascio un dito la visuale scatta, brutto
            {
                Touch touchZero = Input.GetTouch(0);
                Touch touchOne = Input.GetTouch(1);

                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float currentMagnitude = (touchZero.position - touchOne.position).magnitude;

                float difference = currentMagnitude - prevMagnitude;

                zoom(difference * zoomTouchMultiplicator);
            }
            else*/
            {

                float horMovement = Input.GetAxis("Horizontal");
                float verMovement = Input.GetAxis("Vertical");
                cam.transform.position += new Vector3(horMovement, verMovement, 0) * KeySpeedScale * cam.orthographicSize;

                if (Input.GetMouseButton(0))
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        position = GetWorldPosition(Input.mousePosition);
                    }
                    if (Input.GetMouseButton(0))
                    {
                        Vector3 direction = position - GetWorldPosition(Input.mousePosition);
                        direction.z = 0;
                        cam.transform.position += direction;
                    }
                }

                zoom(Input.GetAxis("Mouse ScrollWheel") * zoomScrollWheelMultiplicator);

                limitPosition();
            }

        }
    }

        private Vector3 GetWorldPosition(Vector3 input)
        {
            Ray mousePos = cam.ScreenPointToRay(input);
            float distance;
            ground.Raycast(mousePos, out distance);
            return mousePos.GetPoint(distance);
        }

        void zoom(float increment)
        {//TODO da rifare, il massimo e minimo non sono rispettati e si blocca
         //if(Camera.main.transform.position.y > minHeight && Camera.main.transform.position.y <maxHeight)
         //Camera.main.transform.position += transform.forward*increment;
            cam.orthographicSize = Mathf.Clamp(cam.orthographicSize - increment, scaleMin, scaleMax);

        }

        private void limitPosition()
        {
            if (cam.transform.position.x < 0)
                cam.transform.position = new Vector3(0, cam.transform.position.y, cam.transform.position.z);
            if (cam.transform.position.y < 0)
                cam.transform.position = new Vector3(cam.transform.position.x, 0, cam.transform.position.z);
            if (cam.transform.position.x > maxX)
                cam.transform.position = new Vector3(maxX, cam.transform.position.y, cam.transform.position.z);
            if (cam.transform.position.y > maxY)
                cam.transform.position = new Vector3(cam.transform.position.x, maxY, cam.transform.position.z);
        }
    }
