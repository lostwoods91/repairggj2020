﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FruitPlant : Plant
{

    private Text SeedCountText;

    private void Awake()
    {
        base.Awake();
        SeedCountText = GameObject.FindGameObjectWithTag(MyTags.SEED_TEXT_TAG).GetComponent<Text>();
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(IncrementSeedCount());
    }

    IEnumerator IncrementSeedCount()
    {
        while (true)
        {
            yield return new WaitForSeconds(SeedTick);

            GManager.SeedNumber += SeedRate;
            SeedCountText.text = GManager.SeedNumber + "";

        }
    }
}
