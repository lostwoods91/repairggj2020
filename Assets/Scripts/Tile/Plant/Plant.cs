﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Plant : MonoBehaviour
{
    public string PlantName;

    public bool PendingKill { get; private set; }

    public void InitPlant(Tile InTile)
    {
        Tile = InTile;
    } 

    private int[,] Oxygen;

    public Tile Tile {  get; private set; }

    public void InfluenceArea(Action<Tile> Action)
    {
        GameManager GM = GameManager.Get();

        Action(Tile);

        Tile LeftTile = GM.GetTile(new Vector2Int(Tile.Position.x - 1, Tile.Position.y));
        if (LeftTile) Action(LeftTile);
        Tile RightTile = GM.GetTile(new Vector2Int(Tile.Position.x + 1, Tile.Position.y));
        if (RightTile) Action(RightTile);
        Tile DownTile = GM.GetTile(new Vector2Int(Tile.Position.x, Tile.Position.y - 1));
        if (DownTile) Action(DownTile);
        Tile UpTile = GM.GetTile(new Vector2Int(Tile.Position.x, Tile.Position.y + 1));
        if (UpTile) Action(UpTile);

        Tile LefterTile = GM.GetTile(new Vector2Int(Tile.Position.x - 2, Tile.Position.y));
        if (LefterTile) Action(LefterTile);
        Tile RighterTile = GM.GetTile(new Vector2Int(Tile.Position.x + 2, Tile.Position.y));
        if (RighterTile) Action(RighterTile);
        Tile DownerTile = GM.GetTile(new Vector2Int(Tile.Position.x, Tile.Position.y - 2));
        if (DownerTile) Action(DownerTile);
        Tile UperTile = GM.GetTile(new Vector2Int(Tile.Position.x, Tile.Position.y + 2));
        if (UperTile) Action(UperTile);

        Tile LeftnoTile = GM.GetTile(new Vector2Int(Tile.Position.x - 1, Tile.Position.y + 1));
        if (LeftnoTile) Action(LeftnoTile);
        Tile RightneTile = GM.GetTile(new Vector2Int(Tile.Position.x + 1, Tile.Position.y + 1));
        if (RightneTile) Action(RightneTile);
        Tile DownsoTile = GM.GetTile(new Vector2Int(Tile.Position.x - 1, Tile.Position.y - 1));
        if (DownsoTile) Action(DownsoTile);
        Tile UpseTile = GM.GetTile(new Vector2Int(Tile.Position.x + 1, Tile.Position.y - 1));
        if (UpseTile) Action(UpseTile);
    }

    public float SeedTick;
    public int SeedPrice;
    public int SeedRate;

    protected AudioSource MyAudioSource;
    /*public AudioClip 
        DieSound,
        SufferSound;*/
    private Animator MyAnimator;
    protected GameManager GManager;
    public bool isSuffering = false;

    protected void Awake()
    {
        MyAnimator = GetComponent<Animator>();
        GManager = FindObjectOfType<GameManager>();
        MyAudioSource = GetComponent<AudioSource>();
        PendingKill = false;

    } //Awake   

    public void Die()
    {
        MyAnimator.Play(MyTags.DYING_ANIMATION);
        StopAllCoroutines();
        MyAudioSource.volume = 0.5f;
        MyAudioSource.Play();
        PendingKill = true;
        //Invoke("DyingFinished", 0.5f);

    } //Die

    public void DyingFinished()
    {
        Destroy(gameObject);

    } //DyingFinished

    public void StartSuffering()
    {
        isSuffering = true;
        MyAnimator.Play(MyTags.SUFFERING_ANIMATION);
        //MyAudioSource.clip = SufferSound;
       // MyAudioSource.volume = 0.75f;
       // MyAudioSource.Play();

    } //StartSuffering

    public void StopSuffering()
    {
        isSuffering = false;
        MyAnimator.Play(MyTags.IDLE_ANIMATION);
       // MyAudioSource.Stop();

    } //StopSuffering





} //class
