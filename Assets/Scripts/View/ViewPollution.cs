﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ViewPollution : MonoBehaviour
{
    public Tilemap fog;
    public List<TileBase> fogTiles = new List<TileBase>();
    private GameManager GM;

    private void Awake()
    {
        GM = FindObjectOfType<GameManager>();
    }


    //public void drawPollution()
    ////private void drawPollution(int[,] Draw)
    //{

    //    int xSize = GM.Tiles.GetLength(0);
    //    int ySize = GM.Tiles.GetLength(1);

    //    //int xSize = Draw.GetLength(0);
    //    //int ySize = Draw.GetLength(1);
    //    Vector3Int pos = new Vector3Int();
    //    fog.ClearAllTiles();
    //    for (int x = 0; x < xSize; x++)
    //    {
    //        for (int y = 0; y < ySize; y++)
    //        {

    //            int currpollution = GM.Tiles[x, y].Pollution;
    //            pos = new Vector3Int(y, x, 10);
    //            fog.SetTile(pos, fogTiles[-currpollution]); //bisogna invertire il valore di pollution

    //            //fog.SwapTile();

    //            //fog.SetColor(pos, new Color(1.0f, 1.0f, 1.0f, 0.5f));
    //        }
    //    }
    //}

    public void DrawPollutionOnTile(Tile Tile)
    {
        if (Tile)
        {
            Vector3Int pos = new Vector3Int();
            pos = new Vector3Int(Tile.Position.y, Tile.Position.x, 10);
            int currpollution = Tile.Pollution;
            fog.SetTile(pos, fogTiles[-currpollution]); //bisogna invertire il valore di pollution
        }
    }

}
