﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewUI : MonoBehaviour
{
    public Text seedsText;
    public Slider pollutionPercentageSlider;

    private GameManager GM;

    private void Awake()
    {
        GM = FindObjectOfType<GameManager>();
    }

    public void UIUpdate()
    {
        seedsText.text = GM.SeedNumber.ToString();
        Debug.Log(GM.GetPollutionPercentage());
        float currPrc = GM.GetPollutionPercentage();
        float losePrc = GM.losePercentage;
        float val = ((losePrc-currPrc) /losePrc) ;
        Debug.Log(val);
        pollutionPercentageSlider.value = val;
    }
}
